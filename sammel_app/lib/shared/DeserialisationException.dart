class DeserialisationException implements Exception {
  String message;

  DeserialisationException(this.message);
}
