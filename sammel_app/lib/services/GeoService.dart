import 'dart:io';

import 'package:http_server/http_server.dart';
import 'package:latlong2/latlong.dart';
import 'package:sammel_app/Provisioning.dart';
import 'package:sammel_app/shared/DistanceHelper.dart';
import 'package:poly/poly.dart' as poly;

import 'BackendService.dart';

class GeoService {
  late HttpClient httpClient;

  String nominatimHost = 'nominatim.openstreetmap.org';
  String overpassHost = overpassApi;

  int port = 443;

  GeoService({HttpClient? httpMock}) {
    if (httpMock == null)
      httpClient = HttpClient()
        ..badCertificateCallback = ((X509Certificate cert, String host,
                int port) =>
            host == 'localhost' || host == '10.0.2.2' || host == '127.0.0.1');
  }

  Future<GeoData> getDescriptionToPoint(LatLng point) async {
    Uri url = Uri.https(nominatimHost, 'reverse', {
      'lat': point.latitude.toString(),
      'lon': point.longitude.toString(),
      'format': 'jsonv2'
    });

    var response = await httpClient
        .getUrl(url)
        .then((request) => request.close())
        .then(HttpBodyHandler.processResponse)
        .then((HttpClientResponseBody body) {
      if (body.type != "json") {
        throw WrongResponseFormatException('Get-Request bekommt "${body.type}",'
            ' statt "json" - Response zurück: ${body.body}');
      } else {
        return body;
      }
    });

    if (response.response.statusCode < 200 ||
        response.response.statusCode >= 300)
      throw OsmResponseException(response.body.toString());

    var geodata = response.body;

    if (geodata == null) return GeoData();

    return GeoData.fromJson(geodata);
  }

  Future<List> getPolygonAndDescriptionOfPoint(LatLng point) async {
    var twoMeterLat = DistanceHelper.getLatDiffFromM(point, 10.0);
    var twoMeterLng = DistanceHelper.getLongDiffFromM(point, 10.0);
    var upperRightLat = point.latitude - twoMeterLat;
    var upperRightLng = point.longitude - twoMeterLng;
    var lowerLeftLat = point.latitude + twoMeterLat;
    var lowerLeftLng = point.longitude + twoMeterLng;

    Uri url = Uri.https(overpassHost, 'api/interpreter', {
      'data': Uri.decodeComponent(
          '[timeout:5][out:json];(way[building]($upperRightLat,$upperRightLng,$lowerLeftLat,$lowerLeftLng);); out body geom;')
    });
    var response_convex_building_shape = httpClient
        .getUrl(url)
        .then((request) => request.close())
        .then(HttpBodyHandler.processResponse)
        .then((HttpClientResponseBody body) {
      return body;
    });

    url = Uri.https(overpassHost, 'api/interpreter', {
      'data': Uri.decodeComponent(
          '[timeout:5][out:json];(relation[building]($upperRightLat,$upperRightLng,$lowerLeftLat,$lowerLeftLng);); out body geom;')
    });

    var response_concave_building_shape = await httpClient
        .getUrl(url)
        .then((request) => request.close())
        .then(HttpBodyHandler.processResponse)
        .then((HttpClientResponseBody body) {
      return body;
    });

    var response = await response_convex_building_shape;

    if (response.response.statusCode < 200 ||
        response.response.statusCode >= 300)
      throw OsmResponseException(response.body.toString());

    var buildingDataL = filterApartment(point, response.body['elements']);

    if (buildingDataL.isEmpty) {
      var response = await response_concave_building_shape;

      if (response.response.statusCode < 200 ||
          response.response.statusCode >= 300)
        throw OsmResponseException(response.body.toString());
      buildingDataL = filterApartment(point, response.body['elements']);
    }
    //TODO: capture empty list here
    var buildingData;
    var osmId;
    if (buildingDataL.isNotEmpty) {
      buildingData = buildingDataL[1];
      osmId = buildingDataL[0];
    }

    //TODO solve empty elements list
    return [buildingData, osmId];
  }

  List filterApartment(position, elements) {
    for (var asset in elements) {
      var geom = asset['geometry'];
      if (geom != null) {
        if (poly.Polygon(List<poly.Point<num>>.from(geom.map(
                (latlng) => (poly.Point<num>(latlng['lat'], latlng['lon'])))))
            .contains(position.latitude, position.longitude)) {
          return [
            asset['id'],
            List<LatLng>.from(
                geom.map((latlng) => (LatLng(latlng['lat'], latlng['lon']))))
          ];
        }
      } else {
        var membersList = asset['members'];
        if (membersList != null) {
          for (var member in membersList) {
            if (member['role'] == 'outer') {
              if (poly.Polygon(List<poly.Point<num>>.from(member['geometry']
                      .map((latlng) =>
                          (poly.Point<num>(latlng['lat'], latlng['lon'])))))
                  .contains(position.latitude, position.longitude)) {
                return [
                  asset['id'],
                  List<LatLng>.from(member['geometry']
                      .map((latlng) => (LatLng(latlng['lat'], latlng['lon']))))
                ];
              }
            }
          }
        }
      }
    }
    return [];
  }
}

class GeoData {
  String? name;
  String? street;
  String? number;
  String? postcode;
  String? city;

  GeoData([this.name, this.street, this.number, this.postcode, this.city]);

  GeoData.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    street = json['address'] != null ? json['address']['road'] : null;
    number = json['address'] != null ? json['address']['house_number'] : null;
    postcode = json['address'] != null ? json['address']['postcode'] : null;
    city = json['address'] != null ? json['address']['city'] : null;
  }

  String get description => [
        name,
        [(street), (number)].where((e) => e != null).join(' ')
      ].where((e) => e != null).join(', ');

  String get fullAdress => '${[
        (street),
        (number)
      ].where((e) => e != null).join(' ')}, $postcode $city';
}

class OsmResponseException implements Exception {
  var message;

  OsmResponseException(this.message);
}
