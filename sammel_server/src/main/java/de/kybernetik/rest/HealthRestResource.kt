package de.kybernetik.rest

import de.kybernetik.database.faq.FAQDao
import java.lang.System.getProperty
import java.time.LocalDateTime
import jakarta.annotation.security.PermitAll
import jakarta.ejb.EJB
import jakarta.ejb.Stateless
import jakarta.ws.rs.GET
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.Response

@Path("health")
@Stateless
open class HealthRestResource {

    @EJB
    private lateinit var faqDao: FAQDao

    @GET
    @PermitAll
    @Produces("application/json")
    open fun health(): Response {
        return Response
            .ok()
            .entity(
                Health(
                    status = "lebendig",
                    version = "1.4.4",
                    minClient = "1.2.0+29",
                    modus = getProperty("mode"),
                    faqTimestamp = faqDao.getFAQTimestamp()
                )
            )
            .build()

    }
}

data class  Health(
    val status: String,
    val version: String,
    val minClient: String,
    val modus: String?,
    val faqTimestamp: LocalDateTime?
)