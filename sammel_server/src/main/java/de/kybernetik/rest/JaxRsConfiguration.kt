package de.kybernetik.rest

import jakarta.ws.rs.ApplicationPath
import jakarta.ws.rs.core.Application

@ApplicationPath("/")
class JaxRsConfiguration : Application()
