package de.kybernetik.rest

import de.kybernetik.database.vorbehalte.Vorbehalte
import de.kybernetik.database.vorbehalte.VorbehalteDao
import de.kybernetik.shared.FehlenderWertException
import org.jboss.logging.Logger
import java.time.LocalDate
import jakarta.annotation.security.RolesAllowed
import jakarta.ejb.EJB
import jakarta.ejb.Stateless
import jakarta.ws.rs.Consumes
import jakarta.ws.rs.POST
import jakarta.ws.rs.Path
import jakarta.ws.rs.Produces
import jakarta.ws.rs.core.Context
import jakarta.ws.rs.core.MediaType
import jakarta.ws.rs.core.Response
import jakarta.ws.rs.core.SecurityContext

@Stateless
@Path("vorbehalte")
open class VorbehalteRestRessource {
    private val LOG = Logger.getLogger(VorbehalteRestRessource::class.java)

    @EJB
    private lateinit var dao: VorbehalteDao

    @Context
    private lateinit var context: SecurityContext

    @POST
    @RolesAllowed("user")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    open fun legeNeueVorbehalteAn(vorbehalte: VorbehalteDto): Response {
        LOG.info("Lege neue Vorbehalte an durch ${context.userPrincipal.name}")
        LOG.debug("Vorbehalte: ${vorbehalte.id}, ${vorbehalte.vorbehalte}, ${vorbehalte.datum}, ${vorbehalte.ort}")
        try {
            dao.erzeugeNeueVorbehalte(vorbehalte.convertToVorbehalte(context.userPrincipal.name.toLong()))
        } catch (e: FehlenderWertException) {
            LOG.error(e.message)
            return Response.status(322).entity(RestFehlermeldung(e.message)).build()
        }
        return Response.ok().build()
    }
}

data class VorbehalteDto(
    var id: Long? = null,
    var vorbehalte: String? = null,
    var datum: LocalDate? = null,
    var ort: String? = null
) {
    fun convertToVorbehalte(benutzer: Long): Vorbehalte {
        if (this.datum == null) throw FehlenderWertException("Datum")
        return Vorbehalte(id ?: 0, vorbehalte ?: "", benutzer, datum!!, ort ?: "Unbekannt")
    }

    companion object {
        fun convertFromVorbehalte(vorbehalte: Vorbehalte): VorbehalteDto {
            return VorbehalteDto(
                vorbehalte.id,
                vorbehalte.vorbehalte,
                vorbehalte.datum,
                vorbehalte.ort
            )
        }
    }
}
