package de.kybernetik.database.listlocations

import org.jboss.logging.Logger
import jakarta.ejb.Stateless
import jakarta.inject.Inject
import jakarta.persistence.EntityManager
import jakarta.persistence.PersistenceContext

@Stateless
open class ListLocationDao {
    private val LOG = Logger.getLogger(ListLocationDao::class.java)

    @Inject
    @PersistenceContext(unitName = "mariaDB")
    private lateinit var entityManager: EntityManager

    open fun getActiveListLocations(): List<ListLocation>? {
        LOG.debug("Ermittle Listenorte")
        val query = "select l from ListLocation l where l.active = true"
        val listLocations = entityManager
                .createQuery(query, ListLocation::class.java)
                .resultList
        LOG.debug("Listenorte gefunden: ${listLocations.map { it.name }}")
        return listLocations
    }
}
