package de.kybernetik.database

import jakarta.enterprise.context.ApplicationScoped
import jakarta.enterprise.inject.Produces
import jakarta.persistence.EntityManager
import jakarta.persistence.PersistenceContext

@ApplicationScoped
class MariaDbEntityManagerProducer {
    @PersistenceContext(unitName = "mariaDB")
    private lateinit var entityManager: EntityManager

    @Produces
    fun entityManager(): EntityManager {
        return entityManager
    }
}