package de.kybernetik.database.vorbehalte

import org.jboss.logging.Logger
import jakarta.ejb.Stateless
import jakarta.inject.Inject
import jakarta.persistence.EntityManager
import jakarta.persistence.PersistenceContext

@Stateless
open class VorbehalteDao {
    private val LOG = Logger.getLogger(VorbehalteDao::class.java)

    @Inject
    @PersistenceContext(unitName = "mariaDB")
    private lateinit var entityManager: EntityManager

    open fun erzeugeNeueVorbehalte(vorbehalte: Vorbehalte): Vorbehalte {
        LOG.debug("Speichere Vorbehalte ${vorbehalte.id}")
        entityManager.persist(vorbehalte)
        entityManager.flush()
        return vorbehalte
    }
}