package de.kybernetik.database.faq

import java.time.LocalDateTime
import jakarta.persistence.Column
import jakarta.persistence.Entity
import jakarta.persistence.Id
import jakarta.persistence.Table

@Entity
@Table(name = "FAQ_Timestamp")
class FAQTimestamp @Suppress("unused") constructor() {
    @Id
    @Column
    lateinit var timestamp: LocalDateTime
}
