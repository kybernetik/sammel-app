package de.kybernetik.database.faq

import de.kybernetik.database.termine.TermineDao
import org.jboss.logging.Logger
import java.time.LocalDateTime
import jakarta.ejb.Stateless
import jakarta.inject.Inject
import jakarta.persistence.EntityManager
import jakarta.persistence.NoResultException
import jakarta.persistence.PersistenceContext

@Stateless
open class FAQDao {
    private val LOG = Logger.getLogger(TermineDao::class.java)

    @Inject
    @PersistenceContext(unitName = "mariaDB")
    private lateinit var entityManager: EntityManager

    open fun getAllFAQ(): List<FAQ> {
        val faqs = entityManager.createQuery("select f from FAQ f", FAQ::class.java).resultList
        LOG.debug("${faqs.size} FAQ-Einträge gefunden: ${faqs.map { it.title }}")
        return faqs
    }

    open fun getFAQTimestamp(): LocalDateTime? {
        try {
            return entityManager.createQuery("select t from FAQTimestamp t", FAQTimestamp::class.java).singleResult.timestamp
        } catch (e: NoResultException) {
            return null
        }
    }
}